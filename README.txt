#####[PsychOS Tools README]#####

These tools presented in this repository are for use with PsychOS 3 and 
higher. It is a mixture of software, scripts, and etc.

There are text files that contain URL links and other information 
that can be piped to variables contained within PsychOS-related 
software. For example, the 'psychos-update' script checks for 
available supported repositories and will change the main repo used 
(Repos/Main.txt) to whatever it needs to keep going as well as any 
other script that utilizes the Repos/Main.txt file to know which 
repository still works. It should help extend the life of the 'PsychOS 
Tools' set a little longer and make switching to a different repository 
automatic if needed.

This repository also contains resources and configuration files that 
are sym-linked for use with other software. This makes it easier to add
things such as more palettes to GIMP, Krita, or GrafX2; it may be used 
to add more add-ons to software such as Blender. Or for example, it can
be used to add more joystick settings to QJoyPad. However, it won't be 
used for everything as changing settings or adding more resources 
everytime you update could be very frustrating.

You may also notice personally made software such as RetroGrab and
StreamPi. This makes it much easier to help you keep them up-to-date.
And, I may also add compiled binaries to the Binaries directory with
the same idea in mind, but only those that are hard to find or compile
for yourself. If the binary's license requires source code to be
shipped with it as well, that is what the Source directory is for.

Unless otherwise noted, the software, scripts, and so forth in this
repository fall under THEOUTERLINUX SOFTWARE LICENSE (see LICENSE.txt
file or visit https://theouterlinux.gitlab.io/Projects/LICENSE).
