For whatever reason, the plain text version of the following from a chosen Git repository does not match the encrypted version (an anti-tampering measure) therefore, the following below is a default and may not be accurate. It is more than likely a connection issue, but if you would like, feel free to E-Mail theouterlinux@protonmail.com.

THEOUTERLINUX SOFTWARE LICENSE

Version 1.2, October 18, 2019

Copyright (C) 2019
TheOuterLinux,
All rights reserved.


PREAMBLE

This Preamble is intended to describe, in plain English, the nature, intent, and scope of this License. However, this Preamble is not a part of this License. The legal effect of this License is dependent only upon the terms of the License and not this Preamble.

TheOuterLinux Software License is a slightly edited version the original 4-clause BSD with added conditions and a few elements borrowed and/or edited from the Reciprocal Public License. The reasoning behind this is that there are a lot of people that work really hard on software, regardless of what kind of license you think is best. I honestly don't care what you do with this software as long as you do not redistribute it without permission or change any information or code in regards to original authorship, donations, purchasing, or downloadable content. However, there is a clause for when the Software turns 20 years old in which you may license its appropriate elements (what is legally allowed) under the GNU General Public License to prevent complications in regards to Abandonware.

And now a little something for the lawyers...


TERMS AND CONDITIONS

The use of the Software's source and binary forms, without derived works or redistribution and limits on modification, except in cases of Personal Use, are permitted provided that the following conditions are met:

1. Source code must retain the above copyright 
   notice, this list of conditions, the license
   definitions, contact information, and the 
   following disclaimer.

2. Binary forms must reproduce the above 
   copyright notice, this list of conditions, 
   the license definitions, contact information, 
   and the following disclaimer in the 
   documentation and/or other materials 
   provided with the distribution.

3. The deployment and/or possible serving of 
   this software is only granted through 
   approved vendors or with prior written 
   permission by its original author.

4. All advertising materials mentioning features 
   or use of this software must display the 
   following acknowledgement:

   [Name of software] made possible by 
   "TheOuterLinux."

5. Neither the name of "TheOuterLinux" nor the
   names of the Software's contributors may be 
   used to endorse or promote products derived 
   from this software without specific prior 
   written permission.

6. Any and all provided source code in relation
   to the Software are for research purposes 
   only. Users may not edit the Software in 
   any way that suggests that "TheOuterLinux" 
   is not the original author. If there is 
   an error in regards to the Software, 
   please use the provided contact information 
   for reporting.

7. Users are not permitted to modify any areas of
   the Software regarding donations, purchasing, 
   or downloadable content information regardless
   of editing and/or removing code or the use of
   software or hardware extensions.

8. All fan-art in relation to this Software is
   permitted as long as the artwork is licensed
   as CC BY-NC-ND 3.0. Details found at:
   https://creativecommons.org/licenses/by-nc-nd/
   3.0/legalcode. This essentially means you are
   free to make fan-art but not commercially and
   must give credit.

   Any images, audio, video, or documentation not
   related specifically but are included with 
   the Software are to retain their own licenses 
   and do not follow under TheOuterLinux 
   Software License. Such components can be 
   found in the "Included Third-Party 
   Components" and the "Not Included 
   (Downloaded) Third-Party Components" sections.
   

9. The original and any derived work of this 
   software maintained by the original author 
   and contributors that is twenty years of 
   age or older, be it source or binary, as 
   long as it is not a third-party component 
   or trademarked material that cannot legally 
   be covered under this License, such as 
   logos or patents, unless otherwise stated
   by those with the authority to do so, may 
   thence-forth be re-licensed under an 
   appropriate published version of the 
   GNU GENERAL PUBLIC LICENSE (GPL)
   by the Free Software Foundation with the
   restriction that any versions of the
   Software that is utilized in the form of
   "Software as a Service" must also provide
   publicly downloadable, DRM-free copies 
   of all source and binary forms of the
   Software under the same version of the GPL.
   https://www.gnu.org/licenses/gpl.html. The
   3rd-party vendors of the GPL versions of 
   the Software are to take sole responsibility 
   for the availability of source code and will 
   not depend upon the authors or contributors
   for the Software's maintenance. 
   
   If by chance that the software has never
   had source code made publicly available, 
   you may then release this Software under
   an appropriate version of the MIT License
   with the same conditions found above as
   if a GPL version were possible.
   https://opensource.org/licenses/MIT.


INCLUDED THIRD-PARTY COMPONENTS

  DOSBox logo
  License: GPL v2
  https://www.gnu.org/licenses/old-licenses/gpl-2.0

  Hatari logo
  License: GPL v2
  https://www.gnu.org/licenses/old-licenses/gpl-2.0

  help-about.png
  GNOME Project
  License: CC-BY-SA
  http://creativecommons.org/licenses/by-sa/3.0/
  http://www.gnome.org

  help-license.png (Renamed + Edited)
  Crossed Gavels.svg
  License: Public Domain
  Retrieved from https://commons.wikimedia.org

  help-privacy.png (Renamed + Edited)
  User-privacy-icon.svg
  Gregor Cresnar from the Noun Project
  License: CC-BY-SA 3.0
  http://creativecommons.org/licenses/by-sa/3.0/
  Retrieved from https://commons.wikimedia.org

  J2ME.png (Renamed from phone.png)
  GNOME Project
  License: CC-BY-SA
  http://creativecommons.org/licenses/by-sa/3.0/
  http://www.gnome.org

  Tango icons used...
  Tango Desktop Project
  License: Public Domain
  http://tango.freedesktop.org

NOT INCLUDED (DOWNLOADED) THIRD-PARTY COMPONENTS
      
The lists of downloadable and installable third-party software are ever changing and each software is to retain their own licenses. None of the listed software from within RetroGrab are directly included with RetroGrab and come from various places of the Internet. RetroGrab is more or less a package manager with "bookmarks" to older software and other resources retrieved from lists within a Git repository.
      
From within RetroGrab, every attempt has been made to accurately list each software's license as well as ways for users to request to add information, corrections, removals, or report concerns. Having that said, you download and or install all software and or components by use of RetroGrab at your own risk.

Any custom-made icons for use to help launch third-party software (as many do not provide their own) are to be considered as "Fair Use." https://www.copyright.gov/fair-use/more-info.html

If possible, video links regarding the software are provided from various, publicly availble, 3rd party sources around the Internet. RetroGrab currently does not have the means to display a video's licensing information but because of how RetroGrab utilizes such video for educational purposes, each one should be regarded as "Fair Use." https://www.copyright.gov/fair-use/more-info.html.


LICENSE DEFINITIONS

1.0 General; Applicability & Definitions. This TheOuterLinux Software License Version 1.0 ("License") applies to any programs or other works as well as any and all updates or maintenance releases of said programs or works ("Software") which the Software copyright holder ("Licensor") makes publicly available containing a Notice (hereinafter defined) from the Licensor specifying or allowing use or distribution under the terms of this License. As used in this License and Preamble:

1.1 "Abandonware" is a product, usually software (or hardware), in which the rightsholders are positively indeterminate or uncontactable, and for which official support is no longer available.

1.2 "Contributor" means any person or entity who created or contributed to the creation of an Extension or part of original Software.

1.3 "Deployment" means to use, Serve, sublicense or distribute Licensed Software other than for Your internal Research and/or Personal Use, and includes without limitation, any and all internal use or distribution of Licensed Software within Your business or organization other than for Research and/or Personal Use, as well as direct or indirect sublicensing or distribution of Licensed Software by You to any third party in any form or manner.

1.4 "Derived Works" as used in this License is defined under U.S. copyright law.

1.5 "DRM" and "Digital Rights Management" means restricting a individual's Software usage rights through the use of limiting the number of downloads, the use of online authentication, the use of product keys, the use of changing functionality in the form of negative reinforcement, and/or the use of encryption to hide code and/or features.

1.6 "Extensions" means any Modifications, Derivative Works, or Required Components as those terms are defined in this License.

1.7 "Fan-art" means artwork derived from the Software not endorsed by the original author or contributors of the Software.

1.8 "License" means TheOuterLinux Software License.

1.9 "Modifications" means any additions to or deletions from the substance or structure of (i) a file containing Licensed Software, or (ii) any new file that contains any part of Licensed Software, or (iii) any file which replaces or otherwise alters the original functionality of Licensed Software at runtime.

1.10 "Personal Use" means use of Licensed Software by an individual solely for his or her personal, private and non-commercial purposes. An individual's use of Licensed Software in his or her capacity as an officer, employee, member, independent contractor or agent of a corporation, business or organization (commercial or non-commercial) does not qualify as Personal Use.

1.11 "Research" means investigation or experimentation for the purpose of understanding the nature and limits of the Licensed Software and its potential uses.

1.12 "Serving" means to deliver Licensed Software and/or Your Extensions by means of a computer network to one or more computers for purposes of execution of Licensed Software and/or Your Extensions.

1.13 "Software" means any computer programs or other works as well as any updates or maintenance releases of those programs or works which are distributed publicly by Licensor.

1.14 "Software as a Service" and "SaaS" means Software licensed on a subscription bases that is centrally hosted from a server.

1.15 "Source Code" or "Source" means the preferred form for making modifications to the Licensed Software and/or Your Extensions, including all modules contained therein, plus any associated text, interface definition files, scripts used to control compilation and installation of an executable program or other components required by a third party of average skill to build a running version of the Licensed Software or Your Extensions.

1.16 "Third-party" means any entity other than this Software's authors or
contributors.

1.17 "You," "Your," or "User" means an individual or a legal entity exercising rights under this License. For legal entities, "You," "Your," or "User" includes any entity which controls, is controlled by, or is under common control with, You, where "control" means (a) the power, direct or indirect, to cause the direction or management of such entity, whether by contract or otherwise, or (b) ownership of fifty percent (50%) or more of the outstanding shares or beneficial ownership of such entity.


CONTACT INFORMATION

https://github.com/theouterlinux/contact-or-follow
https://gitlab.com/theouterlinux/contact-or-follow
https://bitbucket.com/theouterlinux/contact-or-follow


DISCLAIMER

"THEOUTERLINUX" IS AN ALIAS TO AN INDIVIDUAL EXERCISING THE RIGHT TO PRIVACY; IT IS NOT INTENDED TO BE USED TO REPRESENT A BUSINESS OR AN ORGANIZATION. ANY LIKENESS IN IT'S NAME, LOGOS, OR OTHER ADVERTISED FORMS RESEMBLING OTHER ENTITIES ARE PURELY COINCIDENTAL.

ALL CHARACTERS AND CORPORATIONS OR ESTABLISHMENTS APPEARING IN THIS SOFTWARE ARE EITHER FICTITIOUS OR FOR EDUCATIONAL PURPOSES ONLY. ANY RESEMBLANCE TO REAL PERSONS, LIVING OR DEAD, IS PURELY COINCIDENTAL.

ALL TRADEMARKS ARE THE SOLE PROPERTY OF THEIR RESPECTIVE OWNERS. UNLESS OTHERWISE NOTIFIED, MENTIONED TRADEMARK HOLDERS ARE NOT AFFILIATED WITH THIS SOFTWARE IN ANY WAY AND THE AUTHORS AND MAINTAINERS OF THIS SOFTWARE DECLARE NO AFFILIATION, SPONSORSHIP, NOR ANY PARTNERSHIPS WITH ANY REGISTERED TRADEMARKS.

INFORMATION DISPLAYED WITHIN THIS SOFTWARE MAY BE SUBJECT TO CHANGE 
WITHOUT PRIOR NOTIFICATION.

THIS SOFTWARE IS PROVIDED BY THEOUTERLINUX "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THEOUTERLINUX BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The developers of RetroGrab DO NOT AND WILL NOT host ANY software or other downloadable/installable materials without first having permission to do so.

Any links or downloads to third-party components or software available from this Software, unless otherwise notified, are only supplied for the purpose of "Fair Use" (107 of the U.S. Copyright Act 1976), such as commentary, criticism, news reporting, research, teaching or scholarship.

Any links or downloads to third-party components or software available from this Software are provided “as is” without warranty of any kind, either expressed or implied and such software is to be used at your own risk.

The use of the third-party components or software links or downloads from this Software are done at your own discretion and risk and with agreement that you will be solely responsible for any damage to your computer system or loss of data that results from such activities. You are solely responsible for adequate protection and backup of the data and equipment used in connection with any of the third-party components or software linked to or downloaded from this Software, and the maintainers of this Software will not be liable for any damages that you may suffer in connection with downloading, installing, using, modifying or distributing third-party components or software. No advice or information, whether oral or written, obtained by the authors or contributors of this Software shall create any warranty for third-party components or software.

Additionally, this Software makes no warranty 
that:

    * The third-party components or software 
      will meet your requirements.
    * The third-party components or software 
      will be uninterrupted, timely, secure, 
      or error-free.
    * The results from the use of the 
      third-party components or software 
      will be effective, accurate or reliable.
    * The quality of the third-party components 
      or software will meet your expectations.
    * If errors or problems occur in connection 
      with a download of the third-party 
      components or software obtained from the 
      links or downloads from this Software, 
      they will be corrected.

The links or downloads to third-party components or software and the related documentation made available through this Software are subject to the following conditions:

    * Third-party components or software could
      include technical or other mistakes, 
      inaccuracies or typographical errors.
    * At any time without prior notice, changes 
      may be made to the links or downloads 
      pointing to third-party components or 
      software or documentation made available 
      from the third-party component or 
      software.
    * The third-party component or software may 
      be out of date, and the authors or 
      contributors of this Software make no 
      commitment to update such materials.
    * The authors and contributors of this 
      Software assume no responsibility for 
      errors or omissions in third-party 
      components or software or documentation 
      available.
    * In no event shall the authors or 
      contributors be liable to you or any third
      parties for any special, punitive, 
      incidental, indirect or consequential 
      damages of any kind, or any damages 
      whatsoever, including, without limitation, 
      those resulting from loss of use, lost 
      data or profits, or any liability, arising 
      out of or in connection with the use of 
      third-party components or software.
