Minimum Requirements:
    * gambas3
    * curl
    * wget
    # openssl
    * grep
    * awk
    * sed
    * coreutils
    * unzip
    * x11-utils
    * 7z (p7zip-full)
    * mpv
    * youtube-dl
    * alien (for certain items)
    * yad (for certain items)

Suggestions:
    * DOSBox
    * Hatari
