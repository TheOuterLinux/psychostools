#####[ RetroGrab OnOffSwitch README ]#####

This is an "On/Off" switch for the program "RetroGrab" so if by chance
I need to prevent people from using it, I can whenever I need to. Why?
There are several reasons for this. Maybe too many people have figured 
out how to abuse this program. Sometimes authors of software decide to 
get nostalgic and the software is no longer "abandonware." Maybe
something really weird is going on and I need to prevent people from
downloading anything at all until it can be fixed. In any case, I can
shut this program "off" when is needed either for your or the author(s)
and/or contributor(s) sakes as to avoid different types of problems.

There is also the description file for having a custom message as to why
the entire program was shut-off to the public.
