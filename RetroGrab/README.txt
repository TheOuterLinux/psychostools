#####[ RetroGrab ]#####

This is a simple program for helping you obtain older software designed 
for various operating systems. This program is designed to be used with 
the GNU/Linux operating system "PsychOS" by TheOuterLinux; however, if 
you have Gambas installed (gambas3), it may run just fine on another 
system.
