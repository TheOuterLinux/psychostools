#!/bin/bash
#
#Title: PsychOS Animation Intro
#Author: TheOuterLinux
#Purpose: To have a one-time animation as a "hello" for new users
#
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
n=0
while [[ $n -le 34 ]]
do
    clear
	cat "$DIR/Frame_$n.txt"
	sleep .1
	n=$(($n+1))
done
sleep 3
echo
echo "This is just a one-time-only intro to say thank you for choosing PsychOS."
echo "If you would like to see this intro again, just use 'psychos-intro.'"
echo
echo "Since this is your first time in the console, would you like to check-out"
echo "the CLIMax feature? This is essentially a command-line menu for those not"
echo -n "used to the TTY/console environment. (yes/No): "
read answer
case $answer in
   y*|Y*) climax ;;
   *) echo
      echo "Well if you change your mind, use 'climax' anytime."
      echo "And, thank you again for choosing PsychOS :)"
      echo
      ;;
esac 
