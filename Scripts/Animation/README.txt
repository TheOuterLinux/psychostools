This animation was created with an awesome ASCII editor called JavE. Use JavE if you want to open and edit the .jmov file, though it's easy to create your own. The .jmov file is not needed to play the animation in the Upgrade script, but I included it because I don't want to lose it.

The played.txt file is used to check if the animation has been played or not to keep it from running each time the menu loads after completing an Upgrade step.
