'''
    Title: img2qb
    Author: TheOuterLinux
    Purpose: To make it easier to convert images to DATA format for
             QuickBASIC projects. Exports as FILENAME.QBD.
             
    Copyright (C) 2019  TheOuterLinux

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.

    img2qb.py requirements:
    ----------------------
        Python2.7 or Python3
        ImageMagick
        PIL (Python 2.7 - 3.5 via pip) or python3-pillowfight (Python 3.6 +)
        numpy
        ptyprocess
        six
        colorama
        
'''

IMPORTANT!

Do not lose the palette256.gif file as it is needed for creating DATA
that use SCREEN 13 mode. Keep it in the same directory as the img2qb.py 
script. I tried to get PIL (Python Image Library) to temporarily create 
a 256 (technically 245) color palette just as it does for the CGA and 
16-color arrays, but the resulting RGB values are wrong and it may be due 
to anti-aliasing, but I don't really know.

When running the script, a temporary directory is created and is printed
on the command-line. Take note of this just in case you need to quit
the script midway before it has a change to do any cleanup for you.

I am not sure if this script will work on Windows or not because I do
not run Windows.
