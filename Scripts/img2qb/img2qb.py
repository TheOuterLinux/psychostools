#!/usr/bin/python
'''
    Title: img2qb
    Author: TheOuterLinux
    Purpose: To make it easier to convert images to DATA format for
             QuickBASIC projects. Exports as FILENAME.QBD.
             
    Copyright (C) 2019  TheOuterLinux

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.

    img2qb.py requirements:
    ----------------------
        Python2.7 or Python3
        ImageMagick
        PIL (Python 2.7 - 3.5 via pip) or python3-pillowfight (Python 3.6 +)
        numpy
        ptyprocess
        six
        colorama
        
'''
import os
from os import system
import sys
import signal
from PIL import Image
import subprocess
import numpy as np
from subprocess import call
import tempfile
from tempfile import gettempdir
from shutil import rmtree
from six.moves import input as raw_input
from colorama import init, Fore, Back, Style

import signal
import time
import sys

def clear(): 
    # check and make call for specific operating system 
    _ = call('clear' if os.name =='posix' else 'cls') 

#Filter ANSI escape sequences out of any text sent to stdout or stderr, 
#and replace them with equivalent Win32 calls if using Windows.
if os.name == 'Windows': init()

#Create temporary directory for palettes.
#This method makes things much more cross-platform.
tmp = os.path.join(gettempdir(), '.{}'.format(hash(os.times())))
os.makedirs(tmp)

# Create a palette file for QuickBASIC 16 colors
# Created using QuickBasic:
#    SCREEN 13
#    DRAW "P#,100 '# is color number"
# ...and then the RGB color values were grabbed with GPick
palette16 = [ 
    0,0,0,       #Color  0: Black
    0,0,170,     #Color  1: Blue
    0,170,0,     #Color  2 Green
    0,170,170,   #Color  3: Cyan
    170,0,0,     #Color  4: Red
    170,0,170,   #Color  5: Magenta
    170,85,0,    #Color  6: Brown
    170,170,170, #Color  7: Light gray
    85,85,85,    #Color  8: Gray
    85,85,255,   #Color  9: Light blue
    85,255,85,   #Color 10: Light green
    85,255,255,  #Color 11: Light cyan
    255,85,85,   #Color 12: Light red
    255,85,255,  #Color 13: Light magenta
    255,255,85,  #Color 14: Yellow
    255,255,255, #Color 15: White
    ] + [0,] * 232 * 3
entries16 = 16
resnp16 = np.arange(entries16,dtype=np.uint8).reshape(entries16,1)
resim16 = Image.fromarray(resnp16, mode='P')
resim16.putpalette(palette16)
resim16 = resim16.resize((4, entries16 * 4), Image.ANTIALIAS)
resim16.save(tmp + '/palette16.gif')

#Create classic black/white/cyan/purple CGA color palette
paletteCGA = [ 
    0,0,0,       #Color  0: Black
    85,255,255,  #Color  1: Cyan
    255,85,255,  #Color  2: Magenta
    255,255,255, #Color  3: White
    255,255,255, #Color  3: White
    ] + [0,] * 232 * 3
entries4 = 4
resnp4 = np.arange(entries4,dtype=np.uint8).reshape(entries4,1)
resim4 = Image.fromarray(resnp4, mode='P')
resim4.putpalette(paletteCGA)
resim4 = resim4.resize((4, entries4 * 4), Image.ANTIALIAS)
resim4.save(tmp + '/paletteCGA.gif')

# Create a palette file for QuickBASIC 256 colors
# Created using QuickBasic:
#    SCREEN 13
#    DRAW "P#,100 '# is color number"
# ...and then the RGB color values were displayed using COLORHLP.BAS
# script I made (https://theouterlinux.gitlab.io/Downloads/COLORHLP.BAS)
''' I'm keeping the following commented until I can figure out why PIL isn't making the palette correctly.
# palette256 = [ 
    # 0,0,0,       #Color   0: Black
    # 0,0,170,     #Color   1: Blue
    # 0,170,0,     #Color   2: Green
    # 0,170,170,   #Color   3: Cyan
    # 170,0,0,     #Color   4: Red
    # 170,0,170,   #Color   5: Magenta
    # 170,85,0,    #Color   6: Brown
    # 170,170,170, #Color   7: Light gray
    # 85,85,85,    #Color   8: Gray
    # 85,85,255,   #Color   9: Light blue
    # 85,255,85,   #Color  10: Light green
    # 85,255,255,  #Color  11: Light cyan
    # 255,85,85,   #Color  12: Light red
    # 255,85,255,  #Color  13: Light magenta
    # 255,255,85,  #Color  14: Yellow
    # 255,255,255, #Color  15: White
    # 20,20,20,    #Color  17: Almost black
    # 32,32,32,    #Color  18: Almost black
    # 45,45,45,    #Color  19: Dark grey
    # 57,57,57,    #Color  20: Dark grey
    # 69,69,69,    #Color  21: Dark grey
    # 81,81,81,    #Color  22: Charcoal grey
    # 97,97,97,    #Color  23: Charcoal grey
    # 113,113,113, #Color  24: Medium grey
    # 130,130,130, #Color  25: Medium grey
    # 146,146,146, #Color  26: Grey
    # 162,162,162, #Color  27: Grey
    # 182,182,182, #Color  28: Silver
    # 202,202,202, #Color  29: Silver
    # 227,227,227, #Color  30: Pale grey
    # 0,0,255,     #Color  32: Primary blue
    # 65,0,255,    #Color  33: Primary blue
    # 125,0,255,   #Color  34: Blue/purple
    # 190,0,255,   #Color  35: Bright purple
    # 255,0,255,   #Color  36: Pink/purple
    # 255,0,190,   #Color  37: Bright pink
    # 255,0,125,   #Color  38: Strong pink
    # 255,0,65,    #Color  39: Neon red
    # 255,0,0,     #Color  40: Fire engine red
    # 255,65,0,    #Color  41: Red orange
    # 255,125,0,   #Color  42: Pumpkin orange
    # 255,190,0,   #Color  43: Marigold
    # 255,255,0,   #Color  44: Yellow
    # 190,255,0,   #Color  45: Yellowgreen
    # 125,255,0,   #Color  46: Bright lime
    # 65,255,0,    #Color  47: Poison green
    # 0,255,0,     #Color  48: Fluro green
    # 0,255,65,    #Color  49: Hot green
    # 0,255,125,   #Color  50: Spearmint
    # 0,255,190,   #Color  51: Greenish cyan
    # 0,255,255,   #Color  52: Cyan
    # 0,190,255,   #Color  53: Bright sky blue
    # 0,125,255,   #Color  54: Clear blue
    # 0,65,255,    #Color  55: Vibrant blue
    # 125,125,255, #Color  56: Cornflower
    # 158,125,255, #Color  57: Periwinkle
    # 190,125,255, #Color  58: Light purple
    # 223,125,255, #Color  59... Putting naming on hold for now.
    # 255,125,255, #Color  60
    # 255,125,223, #Color  61
    # 255,125,190, #Color  62
    # 255,125,158, #Color  63
    # 255,125,125, #Color  64
    # 255,158,125, #Color  65
    # 255,190,125, #Color  66
    # 255,223,125, #Color  67
    # 255,255,125, #Color  68
    # 223,255,125, #Color  69
    # 190,255,125, #Color  70
    # 158,255,125, #Color  71
    # 125,255,125, #Color  72
    # 125,255,158, #Color  73
    # 125,255,190, #Color  74
    # 125,255,223, #Color  75
    # 125,255,255, #Color  76
    # 125,223,255, #Color  77
    # 125,190,255, #Color  78
    # 125,158,255, #Color  79
    # 182,182,255, #Color  80
    # 199,182,255, #Color  81
    # 219,182,255, #Color  82
    # 235,182,255, #Color  83
    # 255,182,255, #Color  84
    # 255,182,235, #Color  85
    # 255,182,219, #Color  86
    # 255,182,199, #Color  87
    # 255,182,182, #Color  88
    # 255,199,182, #Color  89
    # 255,219,182, #Color  90
    # 255,235,182, #Color  91
    # 255,255,182, #Color  92
    # 235,255,182, #Color  93
    # 219,255,182, #Color  94
    # 199,255,182, #Color  95
    # 182,255,182, #Color  96
    # 182,255,199, #Color  97
    # 182,255,219, #Color  98
    # 182,255,235, #Color  99
    # 182,255,255, #Color 100
    # 182,235,255, #Color 101
    # 182,219,255, #Color 102
    # 182,199,255, #Color 103
    # 0,0,113,     #Color 104
    # 28,0,113,    #Color 105
    # 56,0,113,    #Color 106
    # 85,0,113,    #Color 107
    # 113,0,113,   #Color 108
    # 113,0,85,    #Color 109
    # 113,0,56,    #Color 110
    # 113,0,28,    #Color 111
    # 113,0,0,     #Color 112
    # 113,28,0,    #Color 113
    # 113,56,0,    #Color 114
    # 113,85,0,    #Color 115
    # 113,113,0,   #Color 116
    # 85,113,0,    #Color 117
    # 56,113,0,    #Color 118
    # 28,113,0,    #Color 119
    # 0,113,0,     #Color 120
    # 0,113,28,    #Color 121
    # 0,113,56,    #Color 122
    # 0,113,85,    #Color 123
    # 0,113,113,   #Color 124
    # 0,85,113,    #Color 125
    # 0,56,113,    #Color 126
    # 0,28,113,    #Color 127
    # 56,56,113,   #Color 128
    # 69,56,113,   #Color 129
    # 85,56,113,   #Color 130
    # 97,56,113,   #Color 131
    # 113,56,113,  #Color 132
    # 113,56,97,   #Color 133
    # 113,56,85,   #Color 134
    # 113,56,69,   #Color 135
    # 113,56,56,   #Color 136
    # 113,69,56,   #Color 137
    # 113,85,56,   #Color 138
    # 113,97,56,   #Color 139
    # 113,113,56,  #Color 140
    # 97,113.56,   #Color 141
    # 85,113,56,   #Color 142
    # 69,113,56,   #Color 143
    # 56,113,56,   #Color 144
    # 56,113,69,   #Color 145
    # 56,113,85,   #Color 146
    # 56,113,97,   #Color 147
    # 56,113,113,  #Color 148
    # 56,97,113,   #Color 149
    # 56,85,113,   #Color 150
    # 56,69,113,   #Color 151
    # 81,81,113,   #Color 152
    # 89,81,113,   #Color 153
    # 97,81,113,   #Color 154
    # 105,81,113,  #Color 155
    # 113,81,113,  #Color 156
    # 113,81,105,  #Color 157
    # 113,81,97,   #Color 158
    # 113,81,89,   #Color 159
    # 113,81,81,   #Color 160
    # 113,89,81,   #Color 161
    # 113,97,81,   #Color 162
    # 113,105,81,  #Color 163
    # 113,113,81,  #Color 164
    # 105,113,81,  #Color 165
    # 97,113,81,   #Color 166
    # 89,113,81,   #Color 167
    # 81,113,81,   #Color 168
    # 81,113,89,   #Color 169
    # 81,113,97,   #Color 170
    # 81,113,105,  #Color 171
    # 81,113,113,  #Color 172
    # 81,105,113,  #Color 173
    # 81,97,113,   #Color 174
    # 81,89,113,   #Color 175
    # 0,0,65,      #Color 176
    # 16,0,65,     #Color 177
    # 32,0,65,     #Color 178
    # 48,0,65,     #Color 179
    # 65,0,65,     #Color 180
    # 65,0,48,     #Color 181
    # 65,0,32,     #Color 182
    # 65,0,16,     #Color 183
    # 65,0,0,      #Color 184
    # 65,16,0,     #Color 185
    # 65,32,0,     #Color 186
    # 65,48,0,     #Color 187
    # 65,65,0,     #Color 188
    # 48,65,0,     #Color 189
    # 32,65,0,     #Color 190
    # 16,65,0,     #Color 191
    # 0,65,0,      #Color 192
    # 0,65,16,     #Color 193
    # 0,65,32,     #Color 194
    # 0,65,48,     #Color 195
    # 0,65,65,     #Color 196
    # 0,48,65,     #Color 197
    # 0,32,65,     #Color 198
    # 0,16,65,     #Color 199
    # 32,32,65,    #Color 200
    # 40,32,56,    #Color 201
    # 48,32,65,    #Color 202
    # 56,32,65,    #Color 203
    # 65,32,65,    #Color 204
    # 65,32,56,    #Color 205
    # 65,32,48,    #Color 206
    # 65,32,40,    #Color 207
    # 65,32,32,    #Color 208
    # 65,40,32,    #Color 209
    # 65,48,32,    #Color 210
    # 65,56,32,    #Color 211
    # 65,65,32,    #Color 212
    # 56,65,32,    #Color 213
    # 48,65,32,    #Color 214
    # 40,65,32,    #Color 215
    # 32,65,32,    #Color 216
    # 32,65,40,    #Color 217
    # 32,65,48,    #Color 218
    # 32,65,56,    #Color 219
    # 32,65,65,    #Color 220
    # 32,56,65,    #Color 221
    # 32,48,65,    #Color 222
    # 32,40,65,    #Color 223
    # 44,44,65,    #Color 224
    # 48,44,65,    #Color 225
    # 52,44,65,    #Color 226
    # 60,44,65,    #Color 227
    # 65,44,65,    #Color 228
    # 65,44,60,    #Color 229
    # 65,44,52,    #Color 230
    # 65,44,48,    #Color 231
    # 65,44,44,    #Color 232
    # 65,48,44,    #Color 233
    # 65,52,44,    #Color 234
    # 65,60,44,    #Color 235
    # 65,65,44,    #Color 236
    # 60,65,44,    #Color 237
    # 52,65,44,    #Color 238
    # 48,65,44,    #Color 239
    # 44,65,44,    #Color 240
    # 44,65,48,    #Color 241
    # 44,65,52,    #Color 242
    # 44,65,60,    #Color 243
    # 44,65,65,    #Color 244
    # 44,60,65,    #Color 245
    # 44,52,65,    #Color 246
    # 44,48,65,    #Color 247
    # ] + [0,] * 232 * 3
# entries256 = 245
# palette256 = np.random.randint(low=0, high=entries256, size=(entries256 * 3), dtype=np.int)
# palette256 = np.concatenate([palette256, np.zeros(entries256 * 3 - len(palette256))]).astype(np.int).tolist()
# resnp256 = np.arange(entries256,dtype=np.uint8).reshape(entries256,1)
# resim256 = Image.fromarray(resnp256, mode='P')
# resim256.putpalette(palette256)
# resim256 = resim256.convert("P", palette=palette256, colors=8)
# resim256 = resim256.resize((4, entries256 * 4), Image.ANTIALIAS)
# resim256.save(tmp + '/palette256.gif')
'''
DIR = os.path.dirname(os.path.realpath(__file__))
palette256 = DIR+'/palette256.gif'
entries256 = 245

#Clear the terminal screen
clear()

#Title
if os.name == 'Windows':
	myCoolTitle='img2qb.py by TheOuterLinux'
	system('title '+myCoolTitle)
else:
	 sys.stdout.write('\x1b]2;img2qb.py by TheOuterLinux\x07')
print (' ' + Fore.CYAN + Back.CYAN + '+-------------------------------------------------------------------++' + Style.RESET_ALL)
print (' ' + Fore.CYAN + Back.CYAN + '||'+ Fore.MAGENTA + Back.WHITE + ' img2qb.py - Convert a modern image to QuickBasic DATA format     ' + Fore.CYAN + Back.CYAN + '||' + Style.RESET_ALL)
print (' ' + Fore.CYAN + Back.CYAN + '||'+ Fore.MAGENTA + Back.WHITE + ' Author - TheOuterLinux (https://theouterlinux.gitlab.io)         ' + Fore.CYAN + Back.CYAN + '||' + Style.RESET_ALL)
print (' ' + Fore.CYAN + Back.CYAN + '||'+ Fore.MAGENTA + Back.WHITE + ' License - GPL v3 or newer (https://www.gnu.org/licenses/)        ' + Fore.CYAN + Back.CYAN + '||' + Style.RESET_ALL)
#print (' ' + Fore.CYAN + Back.CYAN + '+-------------------------------------------------------------------++' + Style.RESET_ALL)
print (' ' + Fore.CYAN + Back.CYAN + "||"+ Fore.BLACK + Back.CYAN + "           Use 'q' or 'quit' any time to end program.             " + Fore.CYAN + Back.CYAN + "||" + Style.RESET_ALL)
print (' ' + Fore.CYAN + Back.CYAN + ' ' + Back.BLACK)

#Print temporarily created palette locations
print(' ' + Fore.CYAN + Back.CYAN + ' ' + Back.BLACK + ' Temporary folder created in ' + Fore.WHITE + tmp + Fore.CYAN + '.')
print(' ' + Fore.CYAN + Back.CYAN + ' ' + Back.BLACK + '')

# Ask for the full path of the file to convert. The xfce4-terminal
# browser supports drag-and-drop.
filepath = raw_input(' ' + Fore.CYAN + Back.CYAN + ' ' + Back.BLACK + ' File to convert (full path): ' + Style.RESET_ALL)
filepath = filepath.replace("'", '')
    
if filepath in ['q','quit','Q','Quit','QUIT','Exit','EXIT','exit']:
	rmtree(tmp, ignore_errors=True)
	print('')
	exit()
if os.path.exists(filepath):
	print('')
else:
	clear()
	print (' ' + Fore.CYAN + Back.CYAN + '+-------------------------------------------------------------------++' + Style.RESET_ALL)
	print (' ' + Fore.CYAN + Back.CYAN + '||'+ Fore.MAGENTA + Back.WHITE + ' img2qb.py - Convert a modern image to QuickBasic DATA format     ' + Fore.CYAN + Back.CYAN + '||' + Style.RESET_ALL)
	print (' ' + Fore.CYAN + Back.CYAN + '||'+ Fore.MAGENTA + Back.WHITE + ' Author - TheOuterLinux (https://theouterlinux.gitlab.io)         ' + Fore.CYAN + Back.CYAN + '||' + Style.RESET_ALL)
	print (' ' + Fore.CYAN + Back.CYAN + '||'+ Fore.MAGENTA + Back.WHITE + ' License - GPL v3 or newer (https://www.gnu.org/licenses/)        ' + Fore.CYAN + Back.CYAN + '||' + Style.RESET_ALL)
	#print (' ' + Fore.CYAN + Back.CYAN + '+-------------------------------------------------------------------++' + Style.RESET_ALL)
	print (' ' + Fore.CYAN + Back.CYAN + "||"+ Fore.MAGENTA + Back.CYAN + "           Use 'q' or 'quit' any time to end program.             " + Fore.CYAN + Back.CYAN + "||" + Style.RESET_ALL)
	print (' ' + Fore.CYAN + Back.CYAN + ' ' + Back.BLACK + '                                                                    ' + Back.CYAN + ' ' + Style.RESET_ALL)
	print (' ' + Fore.CYAN + Back.CYAN + ' ' + Back.BLACK + ' You must input an existing full file path to use this program.     ' + Back.CYAN + ' ' + Style.RESET_ALL)
	print (' ' + Fore.CYAN + Back.CYAN + ' ' + Back.BLACK + ' Please use one of the following formats:                           ' + Back.CYAN + ' ' + Style.RESET_ALL)
	print (' ' + Fore.CYAN + Back.CYAN + ' ' + Back.BLACK + ' ----------------------------------------                           ' + Back.CYAN + ' ' + Style.RESET_ALL)
	print (' ' + Fore.CYAN + Back.CYAN + ' ' + Back.BLACK + '     *.blp                 *.pcx                                    ' + Back.CYAN + ' ' + Style.RESET_ALL)
	print (' ' + Fore.CYAN + Back.CYAN + ' ' + Back.BLACK + '     *.bmp                 *.pdf                                    ' + Back.CYAN + ' ' + Style.RESET_ALL)
	print (' ' + Fore.CYAN + Back.CYAN + ' ' + Back.BLACK + '     *.cur                 *.png                                    ' + Back.CYAN + ' ' + Style.RESET_ALL)
	print (' ' + Fore.CYAN + Back.CYAN + ' ' + Back.BLACK + '     *.gbr                 *.ppm                                    ' + Back.CYAN + ' ' + Style.RESET_ALL)
	print (' ' + Fore.CYAN + Back.CYAN + ' ' + Back.BLACK + '     *.gif                 *.psd                                    ' + Back.CYAN + ' ' + Style.RESET_ALL)
	print (' ' + Fore.CYAN + Back.CYAN + ' ' + Back.BLACK + '     *.icns                *.tga                                    ' + Back.CYAN + ' ' + Style.RESET_ALL)
	print (' ' + Fore.CYAN + Back.CYAN + ' ' + Back.BLACK + '     *.ico                 *.tif, tiff                              ' + Back.CYAN + ' ' + Style.RESET_ALL)
	print (' ' + Fore.CYAN + Back.CYAN + ' ' + Back.BLACK + '     *.jp2 (JPEG 2000)     *.wal                                    ' + Back.CYAN + ' ' + Style.RESET_ALL)
	print (' ' + Fore.CYAN + Back.CYAN + ' ' + Back.BLACK + '     *.jpg, jpeg           *.webp                                   ' + Back.CYAN + ' ' + Style.RESET_ALL)
	print (' ' + Fore.CYAN + Back.CYAN + ' ' + Back.BLACK + '     *.mic                 *.xbm                                    ' + Back.CYAN + ' ' + Style.RESET_ALL)
	print (' ' + Fore.CYAN + Back.CYAN + ' ' + Back.BLACK + '     *.mps                 *.xpm                                    ' + Back.CYAN + ' ' + Style.RESET_ALL)
	print (' ' + Fore.CYAN + Back.CYAN + ' ' + Back.BLACK + '     *.palm                *.xv                                     ' + Back.CYAN + ' ' + Style.RESET_ALL)
	print (' ' + Fore.CYAN + Back.CYAN + ' ' + Back.BLACK + '                                                                    ' + Back.CYAN + ' ' + Style.RESET_ALL)
	print (' ' + Fore.CYAN + Back.CYAN + '                                                                      ' + Back.BLACK + ' ')
	print ('' + Style.RESET_ALL)
	rmtree(tmp, ignore_errors=True)
	exit()

# Convert file extensions to all lower-case since older FAT file systems
# tend to use all upper-case while newer file systems can use upper and
# lower-case. Avoids the hassle of writing more if-than statements than
# necessary.
fileext = os.path.splitext(filepath.lower())[1]
extcount = len(fileext)

# Get the file name without the full path or extension (in most cases)
base = os.path.basename(filepath)
base = os.path.splitext(base)[0]

# Get the path of the directory the original file is in so we know where 
# to put the converted result.
dirpath = os.path.dirname(filepath)

# The file name and path to use for a preview file
previewfile = dirpath + '/' + base + '_img2qb_preview.gif'

# Prepare the QBASIC file name to make it DOS-friendly by removing
# spaces and shortening the file name to 8 characters. Using 'qbd' as
# the file extension to stand for 'QuickBASIC DATA'. And because it is
# still plain text, it shouldn't trick the computer into thinking it is
# a Quicken file.
dosbase = base.replace(' ', '')
dosbase = dosbase[:8]
dosbase = dosbase.upper()
qbfilepath = tmp + '/' + dosbase + '.QBD'
qbfilepathfinal = dirpath + '/' + dosbase + '.QBD'

# The PIL library supports more formats than this, but only supporting
# the following makes the most sense.
if  fileext in ['.blp', '.bmp', '.cur', '.gbr', '.gif', '.icns', '.ico', \
                '.jp2', '.jpeg', '.jpg', '.mic', '.msp', '.palm', '.pcx', \
                '.pdf', '.png', '.ppm', '.psd', '.tga', '.tif', '.tiff', \
                '.wal', '.webp', '.xbm', '.xpm', '.xv']:
	print (' ' + Fore.CYAN + Back.CYAN + ' ' + Back.BLACK + ' File format ' + Fore.WHITE + fileext + Fore.CYAN + ' supported.')
	print (' ' + Fore.CYAN + Back.CYAN + ' ' + Back.BLACK + '')
else:
	clear()
	print (' ' + Fore.CYAN + Back.CYAN + '+-------------------------------------------------------------------++' + Style.RESET_ALL)
	print (' ' + Fore.CYAN + Back.CYAN + '||'+ Fore.MAGENTA + Back.WHITE + ' img2qb.py - Convert a modern image to QuickBasic DATA format     ' + Fore.CYAN + Back.CYAN + '||' + Style.RESET_ALL)
	print (' ' + Fore.CYAN + Back.CYAN + '||'+ Fore.MAGENTA + Back.WHITE + ' Author - TheOuterLinux (https://theouterlinux.gitlab.io)         ' + Fore.CYAN + Back.CYAN + '||' + Style.RESET_ALL)
	print (' ' + Fore.CYAN + Back.CYAN + '||'+ Fore.MAGENTA + Back.WHITE + ' License - GPL v3 or newer (https://www.gnu.org/licenses/)        ' + Fore.CYAN + Back.CYAN + '||' + Style.RESET_ALL)
	#print (' ' + Fore.CYAN + Back.CYAN + '+-------------------------------------------------------------------++' + Style.RESET_ALL)
	print (' ' + Fore.CYAN + Back.CYAN + "||"+ Fore.MAGENTA + Back.CYAN + "           Use 'q' or 'quit' any time to end program.             " + Fore.CYAN + Back.CYAN + "||" + Style.RESET_ALL)
	print (' ' + Fore.CYAN + Back.CYAN + ' ' + Back.BLACK + '                                                                    ' + Back.CYAN + ' ' + Style.RESET_ALL)
	if extcount == 0: print (' ' + Fore.CYAN + Back.CYAN + ' ' + Back.BLACK + ' You must input an existing full file path to use this program.     ' + Back.CYAN + ' ' + Style.RESET_ALL)
	if extcount == 1: print (' ' + Fore.CYAN + Back.CYAN + ' ' + Back.BLACK + ' File format *' + fileext + ' not supported.                                      ' + Back.CYAN + ' ' + Style.RESET_ALL)
	if extcount == 2: print (' ' + Fore.CYAN + Back.CYAN + ' ' + Back.BLACK + ' File format *' + fileext + ' not supported.                                     ' + Back.CYAN + ' ' + Style.RESET_ALL)
	if extcount == 3: print (' ' + Fore.CYAN + Back.CYAN + ' ' + Back.BLACK + ' File format *' + fileext + ' not supported.                                    ' + Back.CYAN + ' ' + Style.RESET_ALL)
	if extcount == 4: print (' ' + Fore.CYAN + Back.CYAN + ' ' + Back.BLACK + ' File format *' + fileext + ' not supported.                                   ' + Back.CYAN + ' ' + Style.RESET_ALL)
	if extcount == 5: print (' ' + Fore.CYAN + Back.CYAN + ' ' + Back.BLACK + ' File format *' + fileext + ' not supported.                                  ' + Back.CYAN + ' ' + Style.RESET_ALL)
	print (' ' + Fore.CYAN + Back.CYAN + ' ' + Back.BLACK + ' Please use one of the following formats:                           ' + Back.CYAN + ' ' + Style.RESET_ALL)
	print (' ' + Fore.CYAN + Back.CYAN + ' ' + Back.BLACK + ' ----------------------------------------                           ' + Back.CYAN + ' ' + Style.RESET_ALL)
	print (' ' + Fore.CYAN + Back.CYAN + ' ' + Back.BLACK + '     *.blp                 *.pcx                                    ' + Back.CYAN + ' ' + Style.RESET_ALL)
	print (' ' + Fore.CYAN + Back.CYAN + ' ' + Back.BLACK + '     *.bmp                 *.pdf                                    ' + Back.CYAN + ' ' + Style.RESET_ALL)
	print (' ' + Fore.CYAN + Back.CYAN + ' ' + Back.BLACK + '     *.cur                 *.png                                    ' + Back.CYAN + ' ' + Style.RESET_ALL)
	print (' ' + Fore.CYAN + Back.CYAN + ' ' + Back.BLACK + '     *.gbr                 *.ppm                                    ' + Back.CYAN + ' ' + Style.RESET_ALL)
	print (' ' + Fore.CYAN + Back.CYAN + ' ' + Back.BLACK + '     *.gif                 *.psd                                    ' + Back.CYAN + ' ' + Style.RESET_ALL)
	print (' ' + Fore.CYAN + Back.CYAN + ' ' + Back.BLACK + '     *.icns                *.tga                                    ' + Back.CYAN + ' ' + Style.RESET_ALL)
	print (' ' + Fore.CYAN + Back.CYAN + ' ' + Back.BLACK + '     *.ico                 *.tif, tiff                              ' + Back.CYAN + ' ' + Style.RESET_ALL)
	print (' ' + Fore.CYAN + Back.CYAN + ' ' + Back.BLACK + '     *.jp2 (JPEG 2000)     *.wal                                    ' + Back.CYAN + ' ' + Style.RESET_ALL)
	print (' ' + Fore.CYAN + Back.CYAN + ' ' + Back.BLACK + '     *.jpg, jpeg           *.webp                                   ' + Back.CYAN + ' ' + Style.RESET_ALL)
	print (' ' + Fore.CYAN + Back.CYAN + ' ' + Back.BLACK + '     *.mic                 *.xbm                                    ' + Back.CYAN + ' ' + Style.RESET_ALL)
	print (' ' + Fore.CYAN + Back.CYAN + ' ' + Back.BLACK + '     *.mps                 *.xpm                                    ' + Back.CYAN + ' ' + Style.RESET_ALL)
	print (' ' + Fore.CYAN + Back.CYAN + ' ' + Back.BLACK + '     *.palm                *.xv                                     ' + Back.CYAN + ' ' + Style.RESET_ALL)
	print (' ' + Fore.CYAN + Back.CYAN + ' ' + Back.BLACK + '                                                                    ' + Back.CYAN + ' ' + Style.RESET_ALL)
	print (' ' + Fore.CYAN + Back.CYAN + '                                                                      ' + Back.BLACK + ' ')
	print ('' + Style.RESET_ALL)
	rmtree(tmp, ignore_errors=True)
	exit()

# Get the selected image file's width and height
im = Image.open(filepath)
width, height = im.size
colorcount = len(list(filter(None, im.histogram())))
if sys.version_info[0] == 2:
	print (' ' + Fore.CYAN + Back.CYAN + ' ' + Back.BLACK + ' Image width:' + Fore.WHITE), width
	print (' ' + Fore.CYAN + Back.CYAN + ' ' + Back.BLACK + ' Image height:' + Fore.WHITE), height
	print (' ' + Fore.CYAN + Back.CYAN + ' ' + Back.BLACK + ' Image color count:' + Fore.WHITE), colorcount

# Ask what SCREEN mode will be used
print (' ' + Fore.CYAN + Back.CYAN + ' ' + Back.BLACK + '')
print (' ' + Fore.WHITE + Back.CYAN + ' ' + Back.BLACK + ' SCREEN Modes')
print (' ' + Fore.CYAN + Back.CYAN + ' ' + Back.BLACK + ' ------------')
print (' ' + Fore.CYAN + Back.CYAN + ' ' + Back.BLACK + ' SCREEN  ' + Fore.WHITE + ' 1' + Fore.CYAN + ' - 320x200 resolution; CGA; black/white/cyan/magenta colors')
print (' ' + Fore.CYAN + Back.CYAN + ' ' + Back.BLACK + ' SCREEN  ' + Fore.WHITE + ' 2' + Fore.CYAN + ' - 640x200 resolution; black and white                     ')
print (' ' + Fore.CYAN + Back.CYAN + ' ' + Back.BLACK + ' SCREEN  ' + Fore.WHITE + ' 7' + Fore.CYAN + ' - 320x200 resolution; 16 colors                           ')
print (' ' + Fore.CYAN + Back.CYAN + ' ' + Back.BLACK + ' SCREEN  ' + Fore.WHITE + ' 8' + Fore.CYAN + ' - 640x200 resolution; 16 colors                           ')
print (' ' + Fore.CYAN + Back.CYAN + ' ' + Back.BLACK + ' SCREEN  ' + Fore.WHITE + ' 9' + Fore.CYAN + ' - 640x350 resolution; 16 colors                           ')
print (' ' + Fore.CYAN + Back.CYAN + ' ' + Back.BLACK + ' SCREEN ' + Fore.WHITE + ' 11' + Fore.CYAN + ' - 640x480 resolution; black and white                     ')
print (' ' + Fore.CYAN + Back.CYAN + ' ' + Back.BLACK + ' SCREEN ' + Fore.WHITE + ' 12' + Fore.CYAN + ' - 640x480 resolution; 16 colors                           ')
print (' ' + Fore.CYAN + Back.CYAN + ' ' + Back.BLACK + ' SCREEN ' + Fore.WHITE + ' 13' + Fore.CYAN + ' - 320x200 resolution; 256 colors                          ')
print (' ' + Fore.CYAN + Back.CYAN + ' ' + Back.BLACK + '')
screenmode = raw_input(' ' + Fore.CYAN + Back.CYAN + ' ' + Back.BLACK + ' What SCREEN mode do you plan on using? (' + Fore.WHITE + '1,2,..,CGA,q' + Fore.CYAN + '): ' + Style.RESET_ALL)
screenmode = screenmode.lower()
clear()
print(' ' + Fore.CYAN + Back.CYAN + '+-------------------------------------------------------------------++' + Style.RESET_ALL)
print(' ' + Fore.CYAN + Back.CYAN + '||'+ Fore.MAGENTA + Back.WHITE + ' img2qb.py - Convert a modern image to QuickBasic DATA format     ' + Fore.CYAN + Back.CYAN + '||' + Style.RESET_ALL)
print(' ' + Fore.CYAN + Back.CYAN + '||'+ Fore.MAGENTA + Back.WHITE + ' Author - TheOuterLinux (https://theouterlinux.gitlab.io)         ' + Fore.CYAN + Back.CYAN + '||' + Style.RESET_ALL)
print(' ' + Fore.CYAN + Back.CYAN + '||'+ Fore.MAGENTA + Back.WHITE + ' License - GPL v3 or newer (https://www.gnu.org/licenses/)        ' + Fore.CYAN + Back.CYAN + '||' + Style.RESET_ALL)
#print (' ' + Fore.CYAN + Back.CYAN + '+-------------------------------------------------------------------++' + Style.RESET_ALL)
print(' ' + Fore.CYAN + Back.CYAN + "||"+ Fore.MAGENTA + Back.CYAN + "           Use 'q' or 'quit' any time to end program.             " + Fore.CYAN + Back.CYAN + "||" + Style.RESET_ALL)
print('')
print(Style.RESET_ALL)
def screen():
	    print (' ' + Back.CYAN + ' ' + Back.BLACK + Fore.WHITE + ' It seems as though your image is quite large (' + Fore.MAGENTA + '> 160x100' + Fore.WHITE + ').       ' + Back.CYAN + ' ' + Back.MAGENTA + ' ' + Style.RESET_ALL)
	    print (' ' + Back.CYAN + ' ' + Back.BLACK + Fore.WHITE + ' If you plan on creating with ' + Fore.CYAN + 'QB64' + Fore.WHITE + ' and then later opening        ' + Back.CYAN + ' ' + Back.MAGENTA + ' ' + Style.RESET_ALL)
	    print (' ' + Back.CYAN + ' ' + Back.BLACK + Fore.WHITE + " with " + Fore.CYAN +"QB45" + Fore.WHITE + " or " + Fore.CYAN + "QBX" + Fore.WHITE + ", you will get a " + Fore.MAGENTA + "'Module level code too large'  " + Back.CYAN + ' ' + Back.MAGENTA + ' ' + Style.RESET_ALL)
	    print (' ' + Back.CYAN + ' ' + Back.BLACK + Fore.WHITE + ' error and the file will not load. So, it is ' + Fore.MAGENTA + 'suggested that      ' + Back.CYAN + ' ' + Back.MAGENTA + ' ' + Style.RESET_ALL)
	    print (' ' + Back.CYAN + ' ' + Back.BLACK + Fore.WHITE + ' ' + Fore.MAGENTA + 'you shrink the file' + Fore.WHITE + ' and run this script again. However,         ' + Back.CYAN + ' ' + Back.MAGENTA + ' ' + Style.RESET_ALL)
	    print (' ' + Back.CYAN + ' ' + Back.BLACK + Fore.WHITE + ' who am I to stop you. Stretching in the following case refers   ' + Back.CYAN + ' ' + Back.MAGENTA + ' ' + Style.RESET_ALL)
	    print (' ' + Back.CYAN + ' ' + Back.BLACK + Fore.WHITE + ' to ' + Fore.MAGENTA + 'forcing' + Fore.WHITE + ' a width and height of ' + Fore.MAGENTA + screendim + Fore.WHITE + ' as opposed to keeping  ' + Back.CYAN + ' ' + Back.MAGENTA + ' ' + Style.RESET_ALL)
	    print (' ' + Back.CYAN + ' ' + Back.BLACK + Fore.WHITE + ' the original aspect ratio of the image.                         ' + Back.CYAN + ' ' + Back.MAGENTA + ' ' + Style.RESET_ALL)
	    print (' ' + Back.CYAN + '                                                                   ' + Back.MAGENTA + ' ' + Style.RESET_ALL)
	    print (' ' + Back.BLACK + ' ' + Back.MAGENTA + '                                                                   ' + Style.RESET_ALL)
	    print('')
		
#SCREEN 1
if screenmode in ['1','screen 1','cga']:
	screenmode = '1'
	print (' ' + Back.CYAN + '                       ' + Style.RESET_ALL)
	print (' ' + Fore.MAGENTA + Back.CYAN +' ' + Back.WHITE + ' SCREEN 1 selected...' + Back.CYAN + ' ' + Style.RESET_ALL)
	print (' ' + Back.CYAN + '                                                                   ' + Style.RESET_ALL)
	colornumber = 4
	screenw = 320
	screenh = 200
	screendim = str(screenw) + 'x' + str(screenh)
	if width > screenw or height > screenh:
		screen()
		stretchyesno = raw_input(' ' + Fore.CYAN + ' Would you like to force a ' + screendim + ' pixel image? (' + Fore.WHITE + 'y/N,q' + Fore.CYAN + '): ' + Style.RESET_ALL)
		stretchyesno = stretchyesno.lower()
		if stretchyesno in ('y', 'yes'): dimensions = '-resize ' + screendim + '\!'
		elif stretchyesno in ('q','quit','exit'):
			rmtree(tmp, ignore_errors=True)
			print('')
			exit()
		else: dimensions = '-resize ' + screendim
	else:
		stretchyesno = 'no'
		dimensions = ""
	
#SCREEN 2
elif screenmode in ["2", "screen 2"]:
	screenmode = '2'
	print (" SCREEN 2 selected...")
	colornumber = 0
	screenw = 640
	screenh = 200
	screendim = str(screenw) + "x" + str(screenh)
	if width > screenw or height > screenh:
		screen()
		stretchyesno = raw_input(" Would you like to force a " + screendim + " pixel image? (y/N,q): ")
		stretchyesno = stretchyesno.lower()
		if stretchyesno in ('y', 'yes'): dimensions = "-resize " + screendim + "\!"
		elif stretchyesno in ('q','quit','exit'):
			rmtree(tmp, ignore_errors=True)
			print('')
			exit()
		else: dimensions = "-resize " + screendim
	else:
		stretchyesno = 'no'
		dimensions = ""
	
#SCREEN 7
elif screenmode in ["7", "screen 7"]:
	screenmode = '7'
	print (" SCREEN 7 selected...")
	colornumber = 16
	screenw = 320
	screenh = 200
	screendim = str(screenw) + "x" + str(screenh)
	if width > screenw or height > screenh:
		screen()
		stretchyesno = raw_input(" Would you like to force a " + screendim + " pixel image? (y/N,q): ")
		stretchyesno = stretchyesno.lower()
		if stretchyesno in ('y', 'yes'): dimensions = "-resize " + screendim + "\!"
		elif stretchyesno in ('q','quit','exit'):
			rmtree(tmp, ignore_errors=True)
			print('')
			exit()
		else: dimensions = "-resize " + screendim
	else:
		stretchyesno = 'no'
		dimensions = ""
	
#SCREEN 8
elif screenmode in ["8", "screen 8"]:
	screenmode = '8'
	print (" SCREEN 8 selected...")
	colornumber = 16
	screenw = 640
	screenh = 200
	screendim = str(screenw) + "x" + str(screenh)
	if width > screenw or height > screenh:
		screen()
		stretchyesno = raw_input(" Would you like to force a " + screendim + " pixel image? (y/N,q): ")
		stretchyesno = stretchyesno.lower()
		if stretchyesno in ('y', 'yes'): dimensions = "-resize " + screendim + "\!"
		elif stretchyesno in ('q','quit','exit'):
			rmtree(tmp, ignore_errors=True)
			print('')
			exit()
		else: dimensions = "-resize " + screendim
	else:
		stretchyesno = 'no'
		dimensions = ""

#SCREEN 9
elif screenmode in ["9", "screen 9"]:
	screenmode = '9'
	print (" SCREEN 9 selected...")
	colornumber = 16
	screenw = 640
	screenh = 350
	screendim = str(screenw) + "x" + str(screenh)
	if width > screenw or height > screenh:
		screen()
		stretchyesno = raw_input(" Would you like to force a " + screendim + " pixel image? (y/N,q): ")
		stretchyesno = stretchyesno.lower()
		if stretchyesno in ('y', 'yes'): dimensions = "-resize " + screendim + "\!"
		elif stretchyesno in ('q','quit','exit'):
			rmtree(tmp, ignore_errors=True)
			print('')
			exit()
		else: dimensions = "-resize " + screendim
	else:
		stretchyesno = 'no'
		dimensions = ""
		
#SCREEN 11
elif screenmode in ["11", "screen 11"]:
	screenmode = '11'
	print (" SCREEN 11 selected...")
	colornumber = 0
	screenw = 640
	screenh = 480
	screendim = str(screenw) + "x" + str(screenh)
	if width > screenw or height > screenh:
		screen()
		stretchyesno = raw_input(" Would you like to force a " + screendim + " pixel image? (y/N,q): ")
		stretchyesno = stretchyesno.lower()
		if stretchyesno in ('y', 'yes'): dimensions = "-resize " + screendim + "\!"
		elif stretchyesno in ('q','quit','exit'):
			rmtree(tmp, ignore_errors=True)
			print('')
			exit()
		else: dimensions = "-resize " + screendim
	else:
		stretchyesno = 'no'
		dimensions = ""

#SCREEN 12
elif screenmode in ["12", "screen 12"]:
	screenmode = '12'
	print (" SCREEN 12 selected...")
	colornumber = 16
	screenw = 640
	screenh = 480
	screendim = str(screenw) + "x" + str(screenh)
	if width > screenw or height > screenh:
		screen()
		stretchyesno = raw_input(" Would you like to force a " + screendim + " pixel image? (y/N,q): ")
		stretchyesno = stretchyesno.lower()
		if stretchyesno in ('y', 'yes'): dimensions = "-resize " + screendim + "\!"
		elif stretchyesno in ('q','quit','exit'):
			rmtree(tmp, ignore_errors=True)
			print('')
			exit()
		else: dimensions = "-resize " + screendim
	else:
		stretchyesno = 'no'
		dimensions = ""

#SCREEN 13
elif screenmode in ["13", "screen 13"]:
	screenmode = '13'
	print (' ' + Back.CYAN + '                       ' + Style.RESET_ALL)
	print (' ' + Fore.MAGENTA + Back.CYAN +' ' + Back.WHITE + ' SCREEN 13 selected...' + Back.CYAN + ' ' + Style.RESET_ALL)
	print (' ' + Back.CYAN + '                                                                   ' + Style.RESET_ALL)
	colornumber = 256
	screenw = 320
	screenh = 200
	screendim = str(screenw) + "x" + str(screenh)
	if width > screenw or height > screenh:
		screen()
		stretchyesno = raw_input(" Would you like to force a " + screendim + " pixel image? (y/N,q): ")
		stretchyesno = stretchyesno.lower()
		if stretchyesno in ('y', 'yes'): dimensions = "-resize " + screendim + "\!"
		elif stretchyesno in ('q','quit','exit'):
			rmtree(tmp, ignore_errors=True)
			print('')
			exit()
		else: dimensions = "-resize " + screendim
	else:
		stretchyesno = 'no'
		dimensions = ""
		
#QUIT
elif screenmode in ['q','quit','Q','Quit','QUIT','Exit','EXIT','exit']: exit()
	
#If invalid SCREEN option picked	
else:
	screenmode = '13'
	print (' ' + Back.CYAN + '                                     ' + Style.RESET_ALL)
	print (' ' + Fore.MAGENTA + Back.CYAN +' ' + Back.WHITE + ' Invalid option. Using SCREEN 13...' + Back.CYAN + ' ' + Style.RESET_ALL)
	print (' ' + Back.CYAN + '                                                                   ' + Style.RESET_ALL)
	colornumber = 256
	screenw = 320
	screenh = 200
	screendim = str(screenw) + "x" + str(screenh)
	if width > screenw or height > screenh:
		screen()
		stretchyesno = raw_input(Fore.CYAN + " Would you like to force a " + screendim + " pixel image? (" + Fore.WHITE + "y/N,q" + Fore.CYAN + "): " + Fore.WHITE)
		stretchyesno = stretchyesno.lower()
		if stretchyesno in ('y', 'yes'): dimensions = "-resize " + screendim + "\!"
		elif stretchyesno in ('q','quit','exit'):
			rmtree(tmp, ignore_errors=True)
			print('')
			exit()
		else: dimensions = "-resize " + screendim
	else:
		stretchyesno = 'no'
		dimensions = ""

# Convert the original image to a limited color image for previewing
# before converting the preview image to QBASIC DATA format. GIF87 format
# was chosen for the output because it is supported by older and newer
# software where as GIF89 is mostly limited to newer software and a few
# currently maintained DOS image viewers. The image is dithered depending
# on whether or not the image has less than 256 colors before the conversion.
if colornumber == 0:
	if colorcount > colornumber:
		cmd = 'convert ' + dimensions + ' -colors 2 -depth 0 +antialias -remap ' + tmp + '/paletteCGA.gif ' + '-monochrome ' + '"'+filepath+'"' + " GIF87:" + '"'+previewfile+'"'
		subprocess.call(cmd, shell=True)
	else:
		cmd = 'convert ' + dimensions + ' -colors 2 -depth 0 +dither +antialias -remap ' + tmp + '/paletteCGA.gif ' + '-monochrome ' + '"'+filepath+'"' + " GIF87:" + '"'+previewfile+'"'
		subprocess.call(cmd, shell=True)
elif colornumber == 2:
	if colorcount > colornumber:
		cmd = 'convert ' + dimensions + ' -colors 2 -depth 1 +antialias -remap ' + tmp + '/palette16.gif ' + '"'+filepath+'"' + " GIF87:" + '"'+previewfile+'"'
		subprocess.call(cmd, shell=True)
	else:
		cmd = 'convert ' + dimensions + ' -colors 2 -depth 1 +dither +antialias -remap ' + tmp + '/palette16.gif ' + '"'+filepath+'"' + " GIF87:" + '"'+previewfile+'"'
		subprocess.call(cmd, shell=True)
elif colornumber == 4:
	cmd = 'convert ' + dimensions + ' -depth 2 +dither +antialias -remap ' + tmp + '/paletteCGA.gif ' + '"'+filepath+'"' + " GIF87:" + '"'+previewfile+'"'
	subprocess.call(cmd, shell=True)
elif colornumber == 16:
	if colorcount > colornumber:
		cmd = 'convert ' + dimensions + ' -colors ' + str(entries16) + ' -depth 4 -quantize Lab +antialias -remap ' + tmp + '/palette16.gif ' + '"'+filepath+'"' + " GIF87:" + '"'+previewfile+'"'
		subprocess.call(cmd, shell=True)
	else:
		cmd = 'convert ' + dimensions + ' -colors ' + str(entries16) + ' -depth 4 +dither +antialias -quantize Lab -remap ' + tmp + '/palette16.gif ' + '"'+filepath+'"' + " GIF87:" + '"'+previewfile+'"'
		subprocess.call(cmd, shell=True)
elif colornumber == 256:
	if colorcount > colornumber:
		cmd = 'convert ' + dimensions + ' -colors ' + str(entries256) + ' -depth 8 +antialias -remap '+ palette256 + ' "'+filepath+'"' + " GIF87:" + '"'+previewfile+'"'
		subprocess.call(cmd, shell=True)
	else:
		cmd = 'convert ' + dimensions + ' -colors ' + str(entries256) + ' -depth 8 +dither +antialias -remap ' + palette256 + ' "'+filepath+'"' + " GIF87:" + '"'+previewfile+'"'
		subprocess.call(cmd, shell=True)
else:
	if colorcount > colornumber:
		cmd = 'convert ' + dimensions + ' -colors ' + str(entries256) + ' -depth 8 +antialias -remap ' + tmp + '/palette256.gif ' + '"'+filepath+'"' + " GIF87:" + '"'+previewfile+'"'
		subprocess.call(cmd, shell=True)
	else:
		cmd = 'convert ' + dimensions + ' -colors ' + str(entries256) + ' -depth 8 +dither +antialias -remap ' + tmp + '/palette256.gif ' + '"'+filepath+'"' + " GIF87:" + '"'+previewfile+'"'
		subprocess.call(cmd, shell=True)
		


# Ask if the user wants 'DATA' added to the beginning of each line to save
# time if the plan is to copy/pasting file contents.
clear()
print(' ' + Fore.CYAN + Back.CYAN + '+-------------------------------------------------------------------++' + Style.RESET_ALL)
print(' ' + Fore.CYAN + Back.CYAN + '||'+ Fore.MAGENTA + Back.WHITE + ' img2qb.py - Convert a modern image to QuickBasic DATA format     ' + Fore.CYAN + Back.CYAN + '||' + Style.RESET_ALL)
print(' ' + Fore.CYAN + Back.CYAN + '||'+ Fore.MAGENTA + Back.WHITE + ' Author - TheOuterLinux (https://theouterlinux.gitlab.io)         ' + Fore.CYAN + Back.CYAN + '||' + Style.RESET_ALL)
print(' ' + Fore.CYAN + Back.CYAN + '||'+ Fore.MAGENTA + Back.WHITE + ' License - GPL v3 or newer (https://www.gnu.org/licenses/)        ' + Fore.CYAN + Back.CYAN + '||' + Style.RESET_ALL)
#print (' ' + Fore.CYAN + Back.CYAN + '+-------------------------------------------------------------------++' + Style.RESET_ALL)
print(' ' + Fore.CYAN + Back.CYAN + "||"+ Fore.MAGENTA + Back.CYAN + "           Use 'q' or 'quit' any time to end program.             " + Fore.CYAN + Back.CYAN + "||" + Style.RESET_ALL)
print('')
print (' ' + Back.MAGENTA + '                 ' + Style.RESET_ALL)
print (' ' + Fore.MAGENTA + Back.MAGENTA +' ' + Back.WHITE + " Append 'DATA?'" + Back.MAGENTA + ' ' + Style.RESET_ALL)
print (' ' + Back.MAGENTA + '                                                                      ' + Style.RESET_ALL)
print (' ' + Back.MAGENTA + ' ' + Back.BLACK + Fore.WHITE + ' If you are planning to copy/paste the contents of the QBD file to  ' + Back.MAGENTA + ' ' + Back.CYAN + ' ' + Style.RESET_ALL)
print (' ' + Back.MAGENTA + ' ' + Back.BLACK + Fore.WHITE + " a QuickBASIC project, it may be helpful to go ahead and add 'DATA' " + Back.MAGENTA + ' ' + Back.CYAN + ' ' + Style.RESET_ALL)
print (' ' + Back.MAGENTA + ' ' + Back.BLACK + Fore.WHITE + ' to the beginning of each line for you. This also helps if you want ' + Back.MAGENTA + ' ' + Back.CYAN + ' ' + Style.RESET_ALL)
print (' ' + Back.MAGENTA + ' ' + Back.BLACK + Fore.WHITE + ' to load the QDB file contents externally using $INCLUDE.           ' + Back.MAGENTA + ' ' + Back.CYAN + ' ' + Style.RESET_ALL)
print (' ' + Back.MAGENTA + '                                                                      ' + Back.CYAN + ' ' + Style.RESET_ALL)
print (' ' + Back.BLACK + ' ' + Back.CYAN + '                                                                      ' + Style.RESET_ALL)
print('')
appendDATA = raw_input (Fore.CYAN + " Would you like to add 'DATA' to the beginning of each line? (" + Fore.WHITE + "Y/n,q" + Fore.CYAN + "): " + Fore.WHITE)
appendDATA = appendDATA.lower()

# Change the im variable from original to preview image	
im = Image.open(previewfile)
width, height = im.size

# To ensure RGB values will be used
rgb_im = im.convert('RGB')
pixelx = 1
pixely = 1

# Convert the preview image to a QBASIC DATA format
# Pixely is used in the for-loop before the pixelx so as to go line-by-line
# as opposed to column-by-column.
if colornumber == 0 or colornumber == 4:
	with open(qbfilepath, "w+") as outfile:
		for pixely in range(height):
			row = []
			for pixelx in range(width):
				r,g,b = rgb_im.getpixel((pixelx, pixely))
				if [r, g, b] == [0,0,0]: row.append("00")
				elif [r, g, b] == [85,255,255]: row.append("01")
				elif [r, g, b] == [255,85,255]: row.append("02")
				elif [r, g, b] == [255,255,255]: row.append("03")
				else: row.append("00")
			if appendDATA in ('n', 'no'): outfile.write(",".join(row) + '\n')
			elif appendDATA in ('q','quit','exit'):
				rmtree(tmp, ignore_errors=True)
				print('')
				exit()
			else: outfile.write("DATA " + ",".join(row) + '\n')
			
elif colornumber == 2 or colornumber == 16:
	with open(qbfilepath, "w+") as outfile:
		for pixely in range(height):
			row = []
			for pixelx in range(width):
				r,g,b = rgb_im.getpixel((pixelx, pixely))
				if [r, g, b] == [0,0,0]: row.append("00")
				elif [r, g, b] == [0,0,170]: row.append("01")
				elif [r, g, b] == [0,170,0]: row.append("02")
				elif [r, g, b] == [0,170,170]: row.append("03")
				elif [r, g, b] == [170,0,0]: row.append("04")
				elif [r, g, b] == [170,0,170]: row.append("05")
				elif [r, g, b] == [170,85,0]: row.append("06")
				elif [r, g, b] == [170,170,170]: row.append("07")
				elif [r, g, b] == [85,85,85]: row.append("08")
				elif [r, g, b] == [85,85,255]: row.append("09")
				elif [r, g, b] == [85,255,85]: row.append("10")
				elif [r, g, b] == [85,255,255]: row.append("11")
				elif [r, g, b] == [255,85,85]: row.append("12")
				elif [r, g, b] == [255,85,255]: row.append("13")
				elif [r, g, b] == [255,255,85]: row.append("14")
				elif [r, g, b] == [255,255,255]: row.append("15")
				else: row.append("00")
			if appendDATA in ('n', 'no'): outfile.write(",".join(row) + '\n')
			elif appendDATA in ('q','quit','exit'):
				rmtree(tmp, ignore_errors=True)
				print('')
				exit()
			else: outfile.write("DATA " + ",".join(row) + '\n')
			
else:
	with open(qbfilepath, "w+") as outfile:
		for pixely in range(height):
			row = []
			for pixelx in range(width):
				r,g,b = rgb_im.getpixel((pixelx, pixely))
				
				#The following was once used to help correct color palette deviations,
				#but I've decided to make users use the provided 245 color palette
				#instead of generating one because PIL doesn't do a very good
				#job when it comes to too many colors.
				'''
				#Dumb-dowm the r values to match palette...
				# if 0 <= r < 16: r=0
				# elif 16 <= r < 20: r=16
				# elif 20 <= r < 28: r=20
				# elif 28 <= r < 32: r=28
				# elif 32 <= r < 40: r=32
				# elif 40 <= r < 44: r=40
				# elif r==44: r=44
				# elif 45 <= r < 48: r=45
				# elif 48 <= r < 56: r=48
				# elif r==56: r=56
				# elif 57 <= r < 65: r=57
				# elif 65 <= r < 69: r=65
				# elif 69 <= r < 81: r=69
				# elif 81 <= r < 85: r=81
				# elif 85 <= r < 89: r=85
				# elif 89 <= r < 97: r=89
				# elif 97 <= r < 105: r=97
				# elif 105 <= r < 113: r=105
				# elif 113 <= r < 125: r=113
				# elif 125 <= r < 130: r=125
				# elif 130 <= r < 146: r=130
				# elif 146 <= r < 162: r=146
				# elif 162 <= r < 170: r=162
				# elif 170 <= r < 182: r=170
				# elif 182 <= r < 190: r=182
				# elif 190 <= r < 199: r=190
				# elif 199 <= r < 202: r=199
				# elif 202 <= r < 227: r=202
				# elif 227 <= r < 235: r=227
				# elif 235 <= r < 255: r=235
				# elif r==255: r=255
				# else: r=0
				
				# if 0 <= g < 16: g=0
				# elif 16 <= g < 20: g=16
				# elif 20 <= g < 28: g=20
				# elif 28 <= g < 32: g=28
				# elif 32 <= g < 40: g=32
				# elif 40 <= g < 44: g=40
				# elif g==44: g=44
				# elif 45 <= g < 48: g=45
				# elif 48 <= g < 56: g=48
				# elif g==56: g=56
				# elif 57 <= g < 65: g=57
				# elif 65 <= g < 69: g=65
				# elif 69 <= g < 81: g=69
				# elif 81 <= g < 85: g=81
				# elif 85 <= g < 89: g=85
				# elif 89 <= g < 97: g=89
				# elif 97 <= g < 105: g=97
				# elif 105 <= g < 113: g=105
				# elif 113 <= g < 125: g=113
				# elif 125 <= g < 130: g=125
				# elif 130 <= g < 146: g=130
				# elif 146 <= g < 162: g=146
				# elif 162 <= g < 170: g=162
				# elif 170 <= g < 182: g=170
				# elif 182 <= g < 190: g=182
				# elif 190 <= g < 199: g=190
				# elif 199 <= g < 202: g=199
				# elif 202 <= g < 227: g=202
				# elif 227 <= g < 235: g=227
				# elif 235 <= g < 255: g=235
				# elif g==255: g=255
				# else: g=0
				
				# if 0 <= b < 16: b=0
				# elif 16 <= b < 20: b=16
				# elif 20 <= b < 28: b=20
				# elif 28 <= b < 32: b=28
				# elif 32 <= b < 40: b=32
				# elif 40 <= b < 44: b=40
				# elif b==44: b=44
				# elif 45 <= b < 48: b=45
				# elif 48 <= b < 56: b=48
				# elif b==56: b=56
				# elif 57 <= b < 65: b=57
				# elif 65 <= b < 69: b=65
				# elif 69 <= b < 81: b=69
				# elif 81 <= b < 85: b=81
				# elif 85 <= b < 89: b=85
				# elif 89 <= b < 97: b=89
				# elif 97 <= b < 105: b=97
				# elif 105 <= b < 113: b=105
				# elif 113 <= b < 125: b=113
				# elif 125 <= b < 130: b=125
				# elif 130 <= b < 146: b=130
				# elif 146 <= b < 162: b=146
				# elif 162 <= b < 170: b=162
				# elif 170 <= b < 182: b=170
				# elif 182 <= b < 190: b=182
				# elif 190 <= b < 199: b=190
				# elif 199 <= b < 202: b=199
				# elif 202 <= b < 227: b=202
				# elif 227 <= b < 235: b=227
				# elif 235 <= b < 255: b=235
				# elif b==255: b=255
				# else: b=0
				'''
				
				if [r, g, b] == [0,0,0]: row.append("00")
				elif [r, g, b] == [0,0,170]: row.append("01")
				elif [r, g, b] == [0,170,0]: row.append("02")
				elif [r, g, b] == [0,170,170]: row.append("03")
				elif [r, g, b] == [170,0,0]: row.append("04")
				elif [r, g, b] == [170,0,170]: row.append("05")
				elif [r, g, b] == [170,85,0]: row.append("06")
				elif [r, g, b] == [170,170,170]: row.append("07")
				elif [r, g, b] == [85,85,85]: row.append("08")
				elif [r, g, b] == [85,85,255]: row.append("09")
				elif [r, g, b] == [85,255,85]: row.append("10")
				elif [r, g, b] == [85,255,255]: row.append("11")
				elif [r, g, b] == [255,85,85]: row.append("12")
				elif [r, g, b] == [255,85,255]: row.append("13")
				elif [r, g, b] == [255,255,85]: row.append("14")
				elif [r, g, b] == [255,255,255]: row.append("15")
				#elif r==0 and g==0 and b==0: row.append("16")
				elif r==20 and g==20 and b==20: row.append("17")
				elif r==32 and g==32 and b==32: row.append("18")
				elif r==45 and g==45 and b==45: row.append("19")
				elif r==57 and g==57 and b==57: row.append("20")
				elif r==69 and g==69 and b==69: row.append("21")
				elif r==81 and g==81 and b==81: row.append("22")
				elif r==97 and g==97 and b==97: row.append("23")
				elif r==113 and g==113 and b==113: row.append("24")
				elif r==130 and g==130 and b==130: row.append("25")
				elif r==146 and g==146 and b==146: row.append("26")
				elif r==162 and g==162 and b==162: row.append("27")
				elif r==182 and g==182 and b==182: row.append("28")
				elif r==202 and g==202 and b==202: row.append("29")
				elif r==227 and g==227 and b==227: row.append("30")
				#elif r==255 and g==255 and b==255: row.append("31")
				elif r==0 and g==0 and b==255: row.append("32")
				elif r==65 and g==0 and b==255: row.append("33")
				elif r==125 and g==0 and b==255: row.append("34")
				elif r==190 and g==0 and b==255: row.append("35")
				elif r==255 and g==0 and b==255: row.append("36")
				elif r==255 and g==0 and b==190: row.append("37")
				elif r==255 and g==0 and b==125: row.append("38")
				elif r==255 and g==0 and b==65: row.append("39")
				elif r==255 and g==0 and b==0: row.append("40")
				elif r==255 and g==65 and b==0: row.append("41")
				elif r==255 and g==125 and b==0: row.append("42")
				elif r==255 and g==190 and b==0: row.append("43")
				elif r==255 and g==255 and b==0: row.append("44")
				elif r==190 and g==255 and b==0: row.append("45")
				elif r==125 and g==255 and b==0: row.append("46")
				elif r==65 and g==255 and b==0: row.append("47")
				elif r==0 and g==255 and b==0: row.append("48")
				elif r==0 and g==255 and b==65: row.append("49")
				elif r==0 and g==255 and b==125: row.append("50")
				elif r==0 and g==255 and b==190: row.append("51")
				elif r==0 and g==255 and b==255: row.append("52")
				elif r==0 and g==190 and b==255: row.append("53")
				elif r==0 and g==125 and b==255: row.append("54")
				elif r==0 and g==65 and b==255: row.append("55")
				elif r==125 and g==125 and b==255: row.append("56")
				elif r==158 and g==125 and b==255: row.append("57")
				elif r==190 and g==125 and b==255: row.append("58")
				elif r==223 and g==125 and b==255: row.append("59")
				elif r==255 and g==125 and b==255: row.append("60")
				elif r==255 and g==125 and b==223: row.append("61")
				elif r==255 and g==125 and b==190: row.append("62")
				elif r==255 and g==125 and b==158: row.append("63")
				elif r==255 and g==125 and b==125: row.append("64")
				elif r==255 and g==158 and b==125: row.append("65")
				elif r==255 and g==190 and b==125: row.append("66")
				elif r==255 and g==223 and b==125: row.append("67")
				elif r==255 and g==255 and b==125: row.append("68")
				elif r==223 and g==255 and b==125: row.append("69")
				elif r==190 and g==255 and b==125: row.append("70")
				elif r==158 and g==255 and b==125: row.append("71")
				elif r==125 and g==255 and b==125: row.append("72")
				elif r==125 and g==255 and b==158: row.append("73")
				elif r==125 and g==255 and b==190: row.append("74")
				elif r==125 and g==255 and b==223: row.append("75")
				elif r==125 and g==255 and b==255: row.append("76")
				elif r==125 and g==223 and b==255: row.append("77")
				elif r==125 and g==190 and b==255: row.append("78")
				elif r==125 and g==158 and b==255: row.append("79")
				elif r==182 and g==182 and b==255: row.append("80")
				elif r==199 and g==182 and b==255: row.append("81")
				elif r==219 and g==182 and b==255: row.append("82")
				elif r==235 and g==182 and b==255: row.append("83")
				elif r==255 and g==182 and b==255: row.append("84")
				elif r==255 and g==182 and b==235: row.append("85")
				elif r==255 and g==182 and b==219: row.append("86")
				elif r==255 and g==182 and b==199: row.append("87")
				elif r==255 and g==182 and b==182: row.append("88")
				elif r==255 and g==199 and b==182: row.append("89")
				elif r==255 and g==219 and b==182: row.append("90")
				elif r==255 and g==235 and b==182: row.append("91")
				elif r==255 and g==255 and b==182: row.append("92")
				elif r==235 and g==255 and b==182: row.append("93")
				elif r==219 and g==255 and b==182: row.append("94")
				elif r==199 and g==255 and b==182: row.append("95")
				elif r==182 and g==255 and b==182: row.append("96")
				elif r==182 and g==255 and b==199: row.append("97")
				elif r==182 and g==255 and b==219: row.append("98")
				elif r==182 and g==255 and b==235: row.append("99")
				elif r==182 and g==255 and b==255: row.append("100")
				elif r==182 and g==235 and b==255: row.append("101")
				elif r==182 and g==219 and b==255: row.append("102")
				elif r==182 and g==199 and b==255: row.append("103")
				elif r==0 and g==0 and b==113: row.append("104")
				elif r==28 and g==0 and b==113: row.append("105")
				elif r==56 and g==0 and b==113: row.append("106")
				elif r==85 and g==0 and b==113: row.append("107")
				elif r==113 and g==0 and b==113: row.append("108")
				elif r==113 and g==0 and b==85: row.append("109")
				elif r==113 and g==0 and b==56: row.append("110")
				elif r==113 and g==0 and b==28: row.append("111")
				elif r==113 and g==0 and b==0: row.append("112")
				elif r==113 and g==28 and b==0: row.append("113")
				elif r==113 and g==56 and b==0: row.append("114")
				elif r==113 and g==85 and b==0: row.append("115")
				elif r==113 and g==113 and b==0: row.append("116")
				elif r==85 and g==113 and b==0: row.append("117")
				elif r==56 and g==113 and b==0: row.append("118")
				elif r==28 and g==113 and b==0: row.append("119")
				elif r==0 and g==113 and b==0: row.append("120")
				elif r==0 and g==113 and b==28: row.append("121")
				elif r==0 and g==113 and b==56: row.append("122")
				elif r==0 and g==113 and b==85: row.append("123")
				elif r==0 and g==113 and b==113: row.append("124")
				elif r==0 and g==85 and b==113: row.append("125")
				elif r==0 and g==56 and b==113: row.append("126")
				elif r==0 and g==28 and b==113: row.append("127")
				elif r==56 and g==56 and b==113: row.append("128")
				elif r==69 and g==56 and b==113: row.append("129")
				elif r==85 and g==56 and b==113: row.append("130")
				elif r==97 and g==56 and b==113: row.append("131")
				elif r==113 and g==56 and b==113: row.append("132")
				elif r==113 and g==56 and b==97: row.append("133")
				elif r==113 and g==56 and b==85: row.append("134")
				elif r==113 and g==56 and b==69: row.append("135")
				elif r==113 and g==56 and b==56: row.append("136")
				elif r==113 and g==69 and b==56: row.append("137")
				elif r==113 and g==85 and b==56: row.append("138")
				elif r==113 and g==97 and b==56: row.append("139")
				elif r==113 and g==113 and b==56: row.append("140")
				elif r==97 and g==113 and b==56: row.append("141")
				elif r==85 and g==113 and b==56: row.append("142")
				elif r==69 and g==113 and b==56: row.append("143")
				elif r==56 and g==113 and b==56: row.append("144")
				elif r==56 and g==113 and b==69: row.append("145")
				elif r==56 and g==113 and b==85: row.append("146")
				elif r==56 and g==113 and b==97: row.append("147")
				elif r==56 and g==113 and b==113: row.append("148")
				elif r==56 and g==97 and b==113: row.append("149")
				elif r==56 and g==85 and b==113: row.append("150")
				elif r==56 and g==69 and b==113: row.append("151")
				elif r==81 and g==81 and b==113: row.append("152")
				elif r==89 and g==81 and b==113: row.append("153")
				elif r==97 and g==81 and b==113: row.append("154")
				elif r==105 and g==81 and b==113: row.append("155")
				elif r==113 and g==81 and b==113: row.append("156")
				elif r==113 and g==81 and b==105: row.append("157")
				elif r==113 and g==81 and b==97: row.append("158")
				elif r==113 and g==81 and b==89: row.append("159")
				elif r==113 and g==81 and b==81: row.append("160")
				elif r==113 and g==89 and b==81: row.append("161")
				elif r==113 and g==97 and b==81: row.append("162")
				elif r==113 and g==105 and b==81: row.append("163")
				elif r==113 and g==113 and b==81: row.append("164")
				elif r==105 and g==113 and b==81: row.append("165")
				elif r==97 and g==113 and b==81: row.append("166")
				elif r==89 and g==113 and b==81: row.append("167")
				elif r==81 and g==113 and b==81: row.append("168")
				elif r==81 and g==113 and b==89: row.append("169")
				elif r==81 and g==113 and b==97: row.append("170")
				elif r==81 and g==113 and b==105: row.append("171")
				elif r==81 and g==113 and b==113: row.append("172")
				elif r==81 and g==105 and b==113: row.append("173")
				elif r==81 and g==97 and b==113: row.append("174")
				elif r==81 and g==89 and b==113: row.append("175")
				elif r==0 and g==0 and b==65: row.append("176")
				elif r==16 and g==0 and b==65: row.append("177")
				elif r==32 and g==0 and b==65: row.append("178")
				elif r==48 and g==0 and b==65: row.append("179")
				elif r==65 and g==0 and b==65: row.append("180")
				elif r==65 and g==0 and b==48: row.append("181")
				elif r==65 and g==0 and b==32: row.append("182")
				elif r==65 and g==0 and b==16: row.append("183")
				elif r==65 and g==0 and b==0: row.append("184")
				elif r==65 and g==16 and b==0: row.append("185")
				elif r==65 and g==32 and b==0: row.append("186")
				elif r==65 and g==48 and b==0: row.append("187")
				elif r==65 and g==65 and b==0: row.append("188")
				elif r==48 and g==65 and b==0: row.append("189")
				elif r==32 and g==65 and b==0: row.append("190")
				elif r==16 and g==65 and b==0: row.append("191")
				elif r==0 and g==65 and b==0: row.append("192")
				elif r==0 and g==65 and b==16: row.append("193")
				elif r==0 and g==65 and b==32: row.append("194")
				elif r==0 and g==65 and b==48: row.append("195")
				elif r==0 and g==65 and b==65: row.append("196")
				elif r==0 and g==48 and b==65: row.append("197")
				elif r==0 and g==32 and b==65: row.append("198")
				elif r==0 and g==16 and b==65: row.append("199")
				elif r==32 and g==32 and b==65: row.append("200")
				elif r==40 and g==32 and b==65: row.append("201")
				elif r==48 and g==32 and b==65: row.append("202")
				elif r==56 and g==32 and b==65: row.append("203")
				elif r==65 and g==32 and b==65: row.append("204")
				elif r==65 and g==32 and b==56: row.append("205")
				elif r==65 and g==32 and b==48: row.append("206")
				elif r==65 and g==32 and b==40: row.append("207")
				elif r==65 and g==32 and b==32: row.append("208")
				elif r==65 and g==40 and b==32: row.append("209")
				elif r==65 and g==48 and b==32: row.append("210")
				elif r==65 and g==56 and b==32: row.append("211")
				elif r==65 and g==65 and b==32: row.append("212")
				elif r==56 and g==65 and b==32: row.append("213")
				elif r==48 and g==65 and b==32: row.append("214")
				elif r==40 and g==65 and b==32: row.append("215")
				elif r==32 and g==65 and b==32: row.append("216")
				elif r==32 and g==65 and b==40: row.append("217")
				elif r==32 and g==65 and b==48: row.append("218")
				elif r==32 and g==65 and b==56: row.append("219")
				elif r==32 and g==65 and b==65: row.append("220")
				elif r==32 and g==56 and b==65: row.append("221")
				elif r==32 and g==48 and b==65: row.append("222")
				elif r==32 and g==40 and b==65: row.append("223")
				elif r==44 and g==44 and b==65: row.append("224")
				elif r==48 and g==44 and b==65: row.append("225")
				elif r==52 and g==44 and b==65: row.append("226")
				elif r==60 and g==44 and b==65: row.append("227")
				elif r==65 and g==44 and b==65: row.append("228")
				elif r==65 and g==44 and b==60: row.append("229")
				elif r==65 and g==44 and b==52: row.append("230")
				elif r==65 and g==44 and b==48: row.append("231")
				elif r==65 and g==44 and b==44: row.append("232")
				elif r==65 and g==48 and b==44: row.append("233")
				elif r==65 and g==52 and b==44: row.append("234")
				elif r==65 and g==60 and b==44: row.append("235")
				elif r==65 and g==65 and b==44: row.append("236")
				elif r==60 and g==65 and b==44: row.append("237")
				elif r==52 and g==65 and b==44: row.append("238")
				elif r==48 and g==65 and b==44: row.append("239")
				elif r==44 and g==65 and b==44: row.append("240")
				elif r==44 and g==65 and b==48: row.append("241")
				elif r==44 and g==65 and b==52: row.append("242")
				elif r==44 and g==65 and b==60: row.append("243")
				elif r==44 and g==65 and b==65: row.append("244")
				elif r==44 and g==60 and b==65: row.append("245")
				elif r==44 and g==52 and b==65: row.append("246")
				elif r==44 and g==48 and b==65: row.append("247")
				else: row.append("00")

        # Write row to file, joining all values with comma between them
        # and then create a new row after the end of each range of pixelx.
        # If appendDATA is 'yes', add the word 'DATA' to the beginning
        # of each line.
			if appendDATA in ('n', 'no'): outfile.write(",".join(row) + '\n')
			elif appendDATA in ('q','quit','exit'):
				rmtree(tmp, ignore_errors=True)
				print('')
				exit()
			else: outfile.write("DATA " + ",".join(row) + '\n')
			
# Move the img2qb file to same directory as original image. This is not
# done by default in the same directory as the original image because
# it will probably freeze that directory as far as using a GUI file
# manager is concerned because there are a lot of 'echo >>' going on.
cmd = 'mv ' + qbfilepath + ' ' + qbfilepathfinal
subprocess.call(cmd, shell=True)

# Cleanup; deletes the temporary directory this script created at the beginning
rmtree(tmp, ignore_errors=True)
	
print(Style.RESET_ALL + "")
clear()
print(' ' + Fore.CYAN + Back.CYAN + '+-------------------------------------------------------------------++' + Style.RESET_ALL)
print(' ' + Fore.CYAN + Back.CYAN + '||'+ Fore.MAGENTA + Back.WHITE + ' img2qb.py - Convert a modern image to QuickBasic DATA format     ' + Fore.CYAN + Back.CYAN + '||' + Style.RESET_ALL)
print(' ' + Fore.CYAN + Back.CYAN + '||'+ Fore.MAGENTA + Back.WHITE + ' Author - TheOuterLinux (https://theouterlinux.gitlab.io)         ' + Fore.CYAN + Back.CYAN + '||' + Style.RESET_ALL)
print(' ' + Fore.CYAN + Back.CYAN + '||'+ Fore.MAGENTA + Back.WHITE + ' License - GPL v3 or newer (https://www.gnu.org/licenses/)        ' + Fore.CYAN + Back.CYAN + '||' + Style.RESET_ALL)
#print (' ' + Fore.CYAN + Back.CYAN + '+-------------------------------------------------------------------++' + Style.RESET_ALL)
print(' ' + Fore.CYAN + Back.CYAN + "||"+ Fore.MAGENTA + Back.CYAN + "           Program finished. Temporary files cleaned.             " + Fore.CYAN + Back.CYAN + "||" + Style.RESET_ALL)
print('')
print(' ' + Back.CYAN + Fore.BLACK + " Congratulations! Files created:" + Style.RESET_ALL)
print(' ' + Fore.CYAN + "Preview file: " + Style.RESET_ALL + previewfile)
print(' ' + Fore.CYAN + "QuickBASIC DATA file: " + Style.RESET_ALL + dirpath + '/' + Fore.MAGENTA + dosbase + ".QBD" + Style.RESET_ALL + "'")
print("")
print(' ' + Back.CYAN + Fore.BLACK + " If not too familiar with QuickBasic, suggested use is:" + Style.RESET_ALL)
print(" DATA ... " + Fore.CYAN + "'Copy/paste from QBD file" + Style.RESET_ALL)
print(" DATA ... " + Fore.CYAN + "'So on and so forth..." + Style.RESET_ALL)
print("")
print(' ' + Back.CYAN + Fore.BLACK + ' Or, use this to load from the QBD file:' + Style.RESET_ALL)
print(" '$INCLUDE: '" + Fore.MAGENTA + dosbase + ".QBD" + Style.RESET_ALL + "'")
print('')
print(' ' + Back.CYAN + Fore.BLACK + ' Followed by:' + Style.RESET_ALL)
print (" SCREEN " + screenmode + " " + Fore.CYAN + "'Use SCREEN number you picked" + Style.RESET_ALL)
print (" CLS")
if stretchyesno in ('y','yes'):
	print(" FOR Y = 1 TO " + str(screenh) + " " + Fore.CYAN + "' For each row; image height" + Style.RESET_ALL)
	print("   FOR X = 1 TO " + str(screenw) + " " + Fore.CYAN + "' For each column; image width" + Style.RESET_ALL)
else:
	print(" FOR Y = 1 TO " + str(height) + " " + Fore.CYAN + "' For each row; image height" + Style.RESET_ALL)
	print("   FOR X = 1 TO " + str(width) + " " + Fore.CYAN + "' For each column; image width" + Style.RESET_ALL)
print("     READ DotColor")
print("     IF DotColor > 0 THEN     " + Fore.CYAN + "'We can then use this IF-THEN statement to" + Style.RESET_ALL)
print("       PSET (X, Y), DotColor  " + Fore.CYAN + "'make COLOR 00 as a transparent color." + Style.RESET_ALL)
print("     END IF")
print("   NEXT X")
print(" NEXT Y")
print("")

#Return colors back to normal on Windows
if os.name == 'Windows': deinit()
